<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disciplinas_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();		
	}

	public function listar()
	{
		return $this->db->get('disciplina');
	}

	public function tabelaDisciplina()
	{		
    $disciplinas = $this->listar()->result();
    $usuario = isset($this->session->userdata{'usuario'})? $this->session->userdata{'usuario'}:null;

		$opcoes = "";
		foreach ($disciplinas as $disciplina) {
			$opcoes .= "<tr>
						<td > {$disciplina->dis_descricao}</td>
						<td> {$disciplina->dis_creditos}</td>
						<td> {$disciplina->dis_semestre}</td>
						<td> {$disciplina->dis_horario}</td>
						<td> {$disciplina->dis_dia}</td>
						<td> <a href='matriculas/$disciplina->dis_codigo/{$usuario}' button class='btn btn-primary btn-block' data-toogle='modal' data-target='modalmatricula' id='modalmatricula' >Matricular</a>
                        </tr>".PHP_EOL;                       
		}
		return $opcoes;
	}

	



	public function selecionaDisciplina($codigo = null)
	{
		$this->db->where('codigo', $dis_codigo);
		$dados['disciplina'] = $this->db->get('disciplina')->row();
		
		return $dados;
	}
	
	public function inserirAlunoDisciplina($codigodisciplina=null, $codigoaluno=null)
	{
		$dados['ad_alu_codigo'] = $codigoaluno;
		$dados['ad_dis_codigo'] = $codigodisciplina;
		$this->db->insert('aluno_disciplina', $dados);		
	}

	public function minhasdisciplinas(){
    $usuario = isset($this->session->userdata{'usuario'})? $this->session->userdata{'usuario'}:null;
		$this->db
		->select("*")
		->from("disciplina")
		->join('aluno_disciplina', 'ad_dis_codigo = dis_codigo ')
		->join('aluno', 'alu_codigo = ad_alu_codigo ')		
		->where('ad_alu_codigo', $usuario);		
		return $this->db->get();
		

	}

	public function tabelaMinhasDisciplina()
	{		
		$disciplinas = $this->minhasdisciplinas()->result();

		$opcoes = "";
		foreach ($disciplinas as $disciplina) {
			$opcoes .= "<tr>
						<td > {$disciplina->dis_descricao}</td>
						<td > {$disciplina->dis_dia}</td>
						<td > {$disciplina->dis_horario}</td>
						<td> <a href='' button class='btn btn-primary btn-block' data-toogle='modal' data-target='minhasdisciplinas' id='minhasdisciplinas' >Detalhes</a>
                        </tr>".PHP_EOL;                       
		}
		return $opcoes;
	}

}
