<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Atividades_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function atualizar($dados)
	{
		$this->db->where('ati_codigo', $dados['ati_codigo']);
		return $this->db->update('atividade', $dados);
	}
	

	public function listar()
	{
		return $this->db->get('atividade');
	}

	public function tabelaAtividade()
	{
		
		$atividades = $this->listar()->result();

		$opcoes = "";
		foreach ($atividades as $atividade) {
			$opcoes .= "<tr>
						<td > {$atividade->ati_descricao}</td>
						<td> {$atividade->ati_peso}</td>
						<td> {$atividade->ati_status}</td>
						<td> {$atividade->ati_dataentrega}</td>
						<td> {$atividade->ati_dis_codigo}</td>
						<td> {$atividade->ati_arquivo}</td>
						<td> <a href=". base_url('atividades/detalhesAtividade/'.$atividade->ati_codigo . $this->session->userdata('ati_codigo') ) ." button class='btn btn-primary btn-block' value='{$atividade->ati_codigo}' name='detalhes' >Detalhes</a>
                        </tr>".PHP_EOL;                       
		}

		return $opcoes;
	}
	


	public function selecionaAtividade($codigo = null)
	{
		$this->db->where('ati_codigo', $codigo);
		$dados['atividade'] = $this->db->get('atividade')->row();
		
		return $dados;
	}	
	
	
	public function minhasatividades(){
    $usuario = isset($this->session->userdata{'usuario'})? $this->session->userdata{'usuario'}:null;
		$this->db
		->select("*")
		->from("disciplina")
		->join('atividade', 'ati_dis_codigo = dis_codigo ')
		->join('aluno_disciplina', 'ad_dis_codigo = dis_codigo ')
		->join('aluno', 'alu_codigo = ad_alu_codigo ')		
		->where('ad_alu_codigo', $usuario);		
		return $this->db->get();
	}

	public function tabelaAtividadeDisciplina()
	{
		
		$atividades = $this->minhasatividades()->result();

		$opcoes = "";
		foreach ($atividades as $atividade) {
			$opcoes .= "<tr>
						<td > {$atividade->ati_descricao}</td>
						<td> {$atividade->dis_descricao}</td>						
						<td> {$atividade->ati_peso}</td>
						<td> {$atividade->ati_dataentrega}</td>
						<td> {$atividade->ati_status}</td>
                        </tr>".PHP_EOL;                       
		}

		return $opcoes;
	}
}
