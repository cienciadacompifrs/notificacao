<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	
	function validar()
	{

		$dados['alu_matricula'] = $this->input->post("matricula");	
		$dados['alu_senha'] = $this->input->post("senha");	
		$this->db->where("alu_matricula", $dados['alu_matricula']);
		$this->db->where("alu_senha", $dados['alu_senha']);
		$resultado['resultados'] = $this->db->get('aluno')->result();
		if(empty($resultado['resultados']))
		{
			$_SESSION['login_erro'] = "Usuário ou senha inválidos";
			$this->session->unset_userdata('logado');
			return false;
		}		
		else
			$dados['usuario'] = $resultado['resultados'][0]->alu_codigo;
			$this->session->set_userdata($dados);
			return true;     	
	}

	function destroi_sessao()
	{
		$this->session->unset_userdata('usuario');
	}
}