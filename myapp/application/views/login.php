<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tela de Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>application/views/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>application/views/css/main.css">	

</head>
<body>	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
				<form class="login100-form validate-form" method="post" action = "logar">

				<span class="login100-form-title p-b-33">
						Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Matricula is required">
						<input class="input100" type="text" name="matricula" placeholder="Matricula">
						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>

					<div class="wrap-input100 rs1 validate-input" data-validate="Password is required">
						<input class="input100" type="password" name="senha" placeholder="Senha">
						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>

					<div class="container-login100-form-btn m-t-20">
						<button class="login100-form-btn">Acessar</button>
					</div>
				</form>	

				<?php
        		if(isset($_SESSION['login_erro'])){
          		?><div class="alert alert-success text-center" role="alert">
            		<?php echo $_SESSION['login_erro'];
            		unset($_SESSION['login_erro']);
          		} ?>
        		</div>

			</div>
		</div>
	</div>

</body>
</html>
