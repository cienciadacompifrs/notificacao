<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Relatórios</title>

  <!-- Bootstrap core CSS-->
  <link href="<?= base_url() ?>application/views/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?= base_url() ?>application/views/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="<?= base_url() ?>application/views/css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand mr-1" href="principal">Notificação</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Minhas disciplinas">
          <a class="nav-link" href="verminhasdisciplinas">
            <i class="fa fa-fw fa-list"></i>
            <span class="nav-link-text">Minhas disciplinas</span>
          </a>
        </li>
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Disciplinas">
          <a class="nav-link" href="disciplina">
            <i class="fa fa-fw fa-file"></i>
            <span class="nav-link-text">Disciplinas</span>
          </a>
        </li>
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Atividades">
          <a class="nav-link" href="atividade">
            <i class="fa fa-fw fa-list"></i>
            <span class="nav-link-text">Atividades</span>
          </a>
        </li>      

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Relatorios">
          <a class="nav-link" href="relatorios">
            <i class="fa fa-fw fa-list"></i>
            <span class="nav-link-text">Relatórios</span>
          </a>
        </li> 
       
        <li class="nav-item">
          <a href="login/logout" class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Sair</a>
        </li>

      </ul>
    </div>
  </nav> 


  <div class="content-wrapper">
    
    <div class="container-fluid">
    
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a>Relatórios</a>
        </li>        
      </ol>      
      <hr>

      <form action="./relatorios" method="post">
        <div class="form-group row">
          <div class="input-group mb-3 col-8">
            <select name="semestre" class="form-control">
              <option selected value="0">Todos</option>
              <option value="1">1º Semestre</option>
              <option value="2">2º Semestre</option>
              <option value="3">3º Semestre</option>
              <option value="4">4º Semestre</option>
              <option value="5">5º Semestre</option>
              <option value="6">6º Semestre</option>
              <option value="7">7º Semestre</option>
              <option value="8">8º Semestre</option>
              <option value="9">9º Semestre</option>
              <option value="10">10º Semestre</option>
            </select>
            <div class="input-group-append">
              <button class="btn btn-success" type="submit" id="filtrar">Pesquisar</button>
            </div>
          </div>
        </div>
       
      </form>

      <div class="table-responsive col-md-12">
      <table class="table table-striped" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>Matricula</th>
          <th>Aluno</th>
          <th>Disciplina</th>
          <th>Semestre</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($dados as $dado): ?>
        <tr>
          <td><?= $dado['matricula'] ?></td>
          <td><?= $dado['aluno'] ?></td>
          <td><?= $dado['disciplina'] ?></td>
          <td><?= $dado['semestre'] ?></td>
        </tr>
        <?php endforeach; ?>

      </table>
    </div>  


    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="copyright text-center my-auto">
      <span>Copyright © Your Website 2019</span>
      </div>
    </footer>

    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Obrigado por utilizar Notificação!</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Você deseja realmente sair?</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Não</button>
            <a class="btn btn-primary" href="deslogar">Sim</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url() ?>application/views/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url() ?>application/views/vendor/popper/popper.min.js"></script>
    <script src="<?= base_url() ?>application/views/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?= base_url() ?>application/views/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?= base_url() ?>application/views/js/sb-admin.min.js"></script>
  </div>
</body>
</html>