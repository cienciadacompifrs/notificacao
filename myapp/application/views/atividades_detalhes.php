<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Notificação</title>

  <!-- Bootstrap core CSS-->
  <link href="<?= base_url() ?>application/views/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?= base_url() ?>application/views/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="<?= base_url() ?>application/views/css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand mr-1" href="principal">Notificação</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Minhas disciplinas">
          <a class="nav-link" href="principal">
            <i class="fa fa-fw fa-list"></i>
            <span class="nav-link-text">Minhas disciplinas</span>
          </a>
        </li>
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Disciplinas">
          <a class="nav-link" href="disciplina">
            <i class="fa fa-fw fa-file"></i>
            <span class="nav-link-text">Disciplinas</span>
          </a>
        </li>
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Atividades">
          <a class="nav-link" href="atividade">
            <i class="fa fa-fw fa-list"></i>
            <span class="nav-link-text">Atividades</span>
          </a>
        </li> 

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Relatorios">
          <a class="nav-link" href="relatorios">
            <i class="fa fa-fw fa-list"></i>
            <span class="nav-link-text">Relatórios</span>
          </a>
        </li>     
       
        <li class="nav-item">
          <a href="login/logout" class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Sair</a>
        </li>

      </ul>
    </div>
  </nav>
  



  <div class="content-wrapper">
    
    <div class="container-fluid">
    
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a>Atividades</a>
        </li>        
      </ol>      
      <hr>

      <div id="main" class="container-fluid">
      <h2 class="page-header">Atividade <?php echo $atividades['atividade']->ati_codigo?></h2>
  
      <form role="form" method="post" action="<?= base_url('atividade') ?>">
        <input id="codigo" name="codigo" type="hidden" value=" "> 
          <div class="box-body">

            <div class="form-group col-md-12">
              <label for="">Descrição</label>
              <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrição" required="" value="<?php echo $atividades['atividade']->ati_descricao?>" disabled>
            </div>      
 
            <div class="form-group col-md-2">
              <label for="">Peso</label>
              <input type="text" class="form-control" id="peso" name="peso" placeholder="Peso" required="" value="<?php echo $atividades['atividade']->ati_peso?>" disabled>               
            </div>  

            <div class="form-group col-md-2">
              <label for="">Status</label>
              <input type="text" class="form-control" id="ati_status" name="status" placeholder="Status" value="<?php echo $atividades['atividade']->ati_status?>" disabled>
            </div>

            <div class="form-group col-md-2">
              <label for="">Data entrega</label>
              <input type="text" class="form-control" id="ati_dataentrega" name="dataentrega" value="<?php echo $atividades['atividade']->ati_dataentrega?>" disabled>
            </div>

            <div class="form-group col-md-2">
              <label for="">Disciplina</label>
              <input type="text" class="form-control" id="ati_dis_codigo" name="disciplina" placeholder="Disciplina" value="<?php echo $atividades['atividade']->ati_dis_codigo?>" disabled>
            </div>

            <div class="form-group col-md-12">
              <label for="exampleFormControlFile1">Adicionar tarefa</label>
              <input type="file" class="form-control-file" id="exampleFormControlFile1">
            </div>
                   
            <div class="box-footer" align="right">
              <a href=<?= base_url('atividade') ?> class="btn btn-primary">Voltar</a>
              <button type="submit" class="btn btn-primary">Enviar tarefa</button>                
            </div>

          </div>
      </form>
    </div>

    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="copyright text-center my-auto">
      <span>Copyright © Your Website 2019</span>
      </div>
    </footer>

    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Obrigado por utilizar Notificação!</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Você deseja realmente sair?</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Não</button>
            <a class="btn btn-primary" href="deslogar">Sim</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url() ?>application/views/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url() ?>application/views/vendor/popper/popper.min.js"></script>
    <script src="<?= base_url() ?>application/views/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?= base_url() ?>application/views/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?= base_url() ?>application/views/js/sb-admin.min.js"></script>
  </div>
</body>
</html>