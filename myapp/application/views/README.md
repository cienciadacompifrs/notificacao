# Notificacao

Descrição do Sistema: Serigne

Lembrete de Aula :

esse aplicativo vai notificar pessoa quando chega perto horário de aula , a disciplina que ele vai ter e os matérias que ele precisa levar , e avisa se tem tarefas para mandar ou prova ,ou atividade.

* Matricula 

* Semestre


Análise de Requisitos e Funcionalidades - Modelagem Marcel


- O usuário deverá informar a sua matrícula.

- O usuário deverá indicar as disciplinas cursadas no semestre.

- O sistema deverá fornecer os horários das aulas.

- O sistema deverá fornecer os materiais compartilhados em cada disciplina.

- O sistema deverá fornecer as datas de atividades relacionadas as disciplinas.

- O sistema deverá enviar notificações das informações totais das disciplinas cursadas pelo aluno.



*Disciplinas

- Codigo_Disciplina

- Semestre

- Descrição

- Creditos

- Horario_Aulas_Inicio

- Matricula_Aluno

- Materiais



*Atividades_Disciplinas

- Codigo_Atividade

- Descricao_Atividade

- Peso_Atividade

- Arquivos_Upados_Atividade

- Status_Entrega

- Data_Entrega