<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}

	public function index()
	{
		
		$this->load->view('login');
	}

	public function validar()
	{	
		if($this->session->has_userdata('usuario'))
		{
			redirect('verminhasdisciplinas');	
		}
		else
		{
			if ($this->login_model->validar())
			{
				redirect('verminhasdisciplinas');
			}
			else
			{
				redirect('login');
			}
		}
	}

	public function logout()
	{
		$this->login_model->destroi_sessao();
		redirect('login');
	}
}
