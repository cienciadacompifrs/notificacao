<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends CI_Controller {
  
  public function index(){
    $semestre = $this->input->post('semestre');
    if(empty($semestre)){
      $semestre = 0;
    }

    $query = $this->db->query(
      "select 
        aluno.alu_matricula matricula,
        aluno.alu_nome aluno,
        disciplina.dis_descricao disciplina, 
        disciplina.dis_semestre semestre
      from aluno 
      left join aluno_disciplina ald on (ald.ad_alu_codigo = aluno.alu_codigo) 
      left join disciplina on (disciplina.dis_codigo = ald.ad_dis_codigo)
      where disciplina.dis_semestre = $semestre or $semestre = 0");
    $dados['dados'] = $query->result_array();

  	$this->load->view('relatorios', $dados);
  }

}