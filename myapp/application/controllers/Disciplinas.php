<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disciplinas extends CI_Controller {

    public function verdisciplina(){

        $this->load->model('disciplinas_model');
		$dados = ['opcoes_disciplinas' => $this->disciplinas_model->tabelaDisciplina()];		
		
        $this->load->view('disciplinas', $dados);
    }

    public function matricular($codigo=null, $codigo2=null){
        $this->load->model('disciplinas_model');
        $dados['matricula'] = $this->disciplinas_model->inserirAlunoDisciplina($codigo, $codigo2);  
        redirect('matriculado');       
    }

    public function minhasdisciplinas(){
        $this->load->model('disciplinas_model');
		$dados = [
					'opcoes_disciplinas' => $this->disciplinas_model->tabelaMinhasDisciplina()];		
		
        $this->load->view('principal', $dados);
    }

}