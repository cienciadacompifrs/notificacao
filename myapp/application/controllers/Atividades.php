<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Atividades extends CI_Controller {

    public function veratividade(){
        $this->load->model('atividades_model');
		$dados = [
					'opcoes_atividades' => $this->atividades_model->tabelaAtividade()];		
		
        $this->load->view('atividades', $dados);
    }

    public function detalhesAtividade($codigo=null){
        $this->load->model('atividades_model');
        $dados['atividades'] = $this->atividades_model->selecionaAtividade($codigo);
        $this->load->view('atividades_detalhes', $dados);        
    }

    public function detalhesAtividadeDisciplina()
        {
        $this->load->model('atividades_model');
		$dados = [
					'opcoes_atividades' => $this->atividades_model->tabelaAtividadeDisciplina()];		
		
        $this->load->view('atividades', $dados);      
    }

}