-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 14-Maio-2019 às 21:44
-- Versão do servidor: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banco`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

DROP TABLE IF EXISTS `aluno`;
CREATE TABLE IF NOT EXISTS `aluno` (
  `alu_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `alu_nome` varchar(50) NOT NULL,
  `alu_matricula` varchar(10) NOT NULL,
  `alu_senha` varchar(50) NOT NULL,
  PRIMARY KEY (`alu_codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`alu_codigo`, `alu_nome`, `alu_matricula`, `alu_senha`) VALUES
(1, 'Aluno1', '123', '123'),
(3, 'Aluno2', '456', '456');

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno_disciplina`
--

DROP TABLE IF EXISTS `aluno_disciplina`;
CREATE TABLE IF NOT EXISTS `aluno_disciplina` (
  `ad_alu_codigo` int(11) NOT NULL,
  `ad_dis_codigo` int(11) NOT NULL,
  PRIMARY KEY (`ad_alu_codigo`,`ad_dis_codigo`),
  KEY `fk_d_alunodisciplina` (`ad_dis_codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividade`
--

DROP TABLE IF EXISTS `atividade`;
CREATE TABLE IF NOT EXISTS `atividade` (
  `ati_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `ati_descricao` varchar(50) NOT NULL,
  `ati_peso` int(11) NOT NULL,
  `ati_status` varchar(50) DEFAULT NULL,
  `ati_dataentrega` date NOT NULL,
  `ati_arquivo` varchar(50) DEFAULT NULL,
  `ati_dis_codigo` int(11) NOT NULL,
  PRIMARY KEY (`ati_codigo`),
  KEY `fk_atividade` (`ati_dis_codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `atividade`
--

INSERT INTO `atividade` (`ati_codigo`, `ati_descricao`, `ati_peso`, `ati_status`, `ati_dataentrega`, `ati_arquivo`, `ati_dis_codigo`) VALUES
(1, 'Apresentação', 10, NULL, '2019-05-20', NULL, 5),
(2, 'Apresentação', 10, NULL, '2019-05-20', NULL, 5),
(3, 'Prova', 10, NULL, '2019-05-16', NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina`
--

DROP TABLE IF EXISTS `disciplina`;
CREATE TABLE IF NOT EXISTS `disciplina` (
  `dis_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `dis_semestre` int(11) NOT NULL,
  `dis_descricao` varchar(50) NOT NULL,
  `dis_creditos` int(11) NOT NULL,
  `dis_horario` time NOT NULL,
  `dis_dia` varchar(10) NOT NULL,
  `dis_material` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`dis_codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `disciplina`
--

INSERT INTO `disciplina` (`dis_codigo`, `dis_semestre`, `dis_descricao`, `dis_creditos`, `dis_horario`, `dis_dia`, `dis_material`) VALUES
(1, 8, 'Tolerância a falhas', 4, '19:00:00', 'segunda', NULL),
(2, 8, 'Tolerância a falhas', 4, '19:00:00', 'segunda', NULL),
(3, 8, 'Sistemas Distribuídos', 4, '19:00:00', 'terça', NULL),
(4, 8, 'Sistemas Distribuídos', 4, '19:00:00', 'terça', NULL),
(5, 8, 'Segurança TI', 4, '19:00:00', 'quarta', NULL),
(6, 8, 'Segurança TI', 4, '19:00:00', 'quarta', NULL),
(7, 8, 'TAES', 4, '19:00:00', 'quinta', NULL),
(8, 8, 'TCC', 4, '19:00:00', 'sexta', NULL),
(9, 8, 'TCC', 4, '19:00:00', 'sexta', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
